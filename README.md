# PM-2022-1

### Organização inicial
- [ ] Caio - `UC01 ao UC04`
- [ ] Wirz - `UC05 ao UC08`
- [ ] Victor - `UC09 ao UC12`
- [ ] Leone - `UC13 ao UC16`

## Tarefa 1
    swagger.yaml

---

## Tarefa 2

#### UC01 – Cadastrar Ciclista
    Microsserviço Ciclista
        verificarEmailCiclista
        cadastrarCiclista [R1, R2]
    
#### UC02 – Confirmar email
    Microsserviço Ciclista
        confirmarEmailCiclista


#### UC03 – Alugar bicicleta
	Microsserviço AluguelBicicleta
        verificarPermissao[R1]
        desbloquearBicicleta[R2. R3. R4. R5]

	Microsserviço Equipamento
        listarBicicletas

#### UC04 – Devolver bicicleta
    Microsserviço AluguelBicicleta
	    devolverBicicleta[R1, R2, R3]

#### UC05 – Autenticar
    *Retirado


#### UC06 – Alterar Dados do Ciclista
    Microsserviço Ciclista
        formAlteraDados[R1, R2]

#### UC07 – Alterar Cartão
    Microsserviço Ciclista
        alterarDadosCartao

#### UC08 – Incluir Bicicleta na Rede de Totens
    Microsserviço Equipamento
        incluirBicicletaTotem[R1, R2]

#### UC09 – Retirar Bicicleta do Sistema de Totens Reparo
    Microsserviço Equipamento
        retirarBicicletaTotem[R1, R2]

#### UC10 – Manter Cadastro de Bicicletas
	Microsserviço Equipamento
        listarBicicletas[R4]
        manterBicicleta[R1, R2, R5]
        editarBicicleta[R2, R3]
        excluirBicicleta

#### UC11 –  Incluir Nova Tranca em Totem
	Microsserviço Equipamento
        listarTranca
        incluirTrancaTotem[R1, R2]

#### UC12 – Retirar Tranca do Sistema de Totens Reparo
	Microsseviço Equipamento
        removerTrancaTotem[R1, R2]

#### UC13 – Manter Cadastro de Trancas
	Microsseviço Equipamento
        listarTranca[R4]
        cadastrarTranca[R1,R2, R3]
        excluirTranca
        editarTranca[R2]

#### UC14 – Manter Cadastro de Totens
	Microsseviço Equipamento
        listarTranca
        listarBicicletas
        listarTotem[R3]
        cadastrarTotem[R1, R2]
        excluirTotem

#### UC15 – Manter Cadastro de Funcionário
	Microsseviço Funcionario
	    listarFuncionario
	    cadastrarFuncionario[R1, R2, R3]
        editarFuncionario[R1, R2]
        excluirFuncionario

#### UC16 – Cobrar Taxas Atrasadas
	Microsserviço AluguelBicicleta
        cobrarTaxa[R1. R2]

---

## Tarefa 3
Caio Rodrigues Dias de Jesus - [Gitlab](https://gitlab.com/caio.jesus)